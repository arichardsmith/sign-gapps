/**
 * Serve current sign status as JSON
 */

const STATUS_COLUMN = 2 // Default column if none found
const DEFAULT_STATUS = 'Closed' // Default trail status

/**
 * Server entry, must return `GoogleAppsScript.Content.TextOutput`
 */
function doGet(): GoogleAppsScript.Content.TextOutput {
  const data = getData()

  return serveJSON(data)
}

/**
 * Return a javascript object as a JSON response
 */
function serveJSON(obj: object) {
  const textContent = JSON.stringify(obj)
  return ContentService.createTextOutput(textContent).setMimeType(
    ContentService.MimeType.JSON
  )
}

type MatrixCell = {
  value: string | null
  validation: GoogleAppsScript.Spreadsheet.DataValidation
}

type Matrix = MatrixCell[][]

/**
 * Gets all data in the spreadsheet, removing empty rows and columns
 */
function getData() {
  const sourceSheet = SpreadsheetApp.getActiveSpreadsheet().getSheets()[0]
  const dataRange = sourceSheet.getDataRange()

  const matrix = buildMatrix(dataRange)

  // Remove empty rows
  const dataMatrix = matrix.filter(notEmptyRow)

  // Work out which column has the statuses in it
  const statusColumn = findStatusColumn(dataMatrix)

  // Remove header and extract the column
  const statuses = dataMatrix.slice(1).map(mapToColumn(statusColumn))

  // Normalise statuses
  const normalisedStatuses = statuses.map(normaliseStatus)
  return normalisedStatuses
}

/**
 * Normalises data into a value, validation matrix
 */
function buildMatrix(range: GoogleAppsScript.Spreadsheet.Range): Matrix {
  const values = range.getValues()
  const validations = range.getDataValidations()

  // Map cells to a MatrixCell
  return values.map((row, rowNo) =>
    row.map((cell, colNo) => ({
      value: typeof cell === 'string' ? cell : null, // Discard non-string cells
      validation: validations[rowNo][colNo]
    }))
  )
}

/**
 * Checks if a row is not empty.
 * Returns false if any cells have a value
 */
function notEmptyRow(row: MatrixCell[]) {
  for (let i = 0; i < row.length; i++) {
    // Clear whitespace and check if cell is empty
    if (row[i].value.trim().length > 0) {
      return true
    }
  }

  return false
}

/**
 * Looks for the status column.
 * First looks for a header with the text `status`,
 * next looks for a column where a cell has a `STATUS_MESSAGES` string
 * Finally defaults to STATUS_COLUMN
 */
function findStatusColumn(arr: Matrix) {
  const headerRow = arr[0]

  // Check all cells in the header row for one containing `status`
  for (let i = 0; i < headerRow.length; i++) {
    const cell = headerRow[i]
    if (
      typeof cell.value === 'string' &&
      cell.value.toLowerCase().indexOf('status') >= 0
    ) {
      return i
    }
  }

  /**
   * Check all remaining rows for cell with the default status,
   * or validation rules that allow the default
   */
  const dataRows = arr.slice(1)
  for (let x = 0; x < dataRows.length; x++) {
    for (let y = 0; y < dataRows[x].length; y++) {
      const cell = dataRows[x][y]
      if (
        (typeof cell.value === 'string' && cell.value === DEFAULT_STATUS) ||
        isStatusCell(cell)
      ) {
        return y
      }
    }
  }

  // Return the default if nothing found
  return STATUS_COLUMN
}

/**
 * Extracts the cell at a set column from a row
 */
function mapToColumn(colIndex: number) {
  return (row: MatrixCell[]) => row[colIndex]
}

/**
 * Checks data validation rules to decide if cell is a status cell
 */
function isStatusCell(cell: MatrixCell) {
  return cell.validation !== null && defaultIsValid(cell.validation)
}

/**
 * Check if default status is valid with given validation rules
 */
function defaultIsValid(
  validation: GoogleAppsScript.Spreadsheet.DataValidation
) {
  if (validation === null) {
    // Default will be valid if no validation rules exist
    return true
  }
  const rules = validation.getCriteriaType()

  if (rules !== SpreadsheetApp.DataValidationCriteria.VALUE_IN_LIST) {
    // Wrong validation rules
    return false
  }

  const validText = validation.getCriteriaValues()[0]

  return validText.indexOf(DEFAULT_STATUS) !== -1
}

/**
 * Normalises status text, falling back to default if invalid value
 */
function normaliseStatus(cell: MatrixCell): string {
  if (isStatusCell(cell)) {
    // Is a status cell, return default if current value is not valid
    if (cell.validation.getCriteriaValues()[0].indexOf(cell.value) === -1) {
      return DEFAULT_STATUS
    }
  }

  return cell.value
}
