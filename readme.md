# Mt Hutt signboard Google apps script

Google apps script to support a google sheet controled electronic signboard.
The scripts provide:

- A web api for the sign to access

## Development

Development environment requires `node`. Build the apps scripts from typescript sources by running `npm run build`.
Deployment is handled by `@google/clasp`, you'll need to login to clasp's cli then you can run `npm run push` to upload files.
